import { getYearMonDateInt } from '@/utils/index'

export const mixinlistMixed={
  data(){
    return{
      datas: [],
    /**分页 **/
    filter: {
        page: 1,
        per_page: 10
      },
      /**总数**/
      total_rows: 0,
      /**加载动作**/
      loading: false,
       /**查看**/
      dialogViewVisible:false,
      dialogAddVisible:false,
      dialogEditVisible:false,
      currentId:'',
      random:0,

      //记录弹框id
      listBox:[],
    }
  },
  methods: {
    /***列表的回调事件 开始**/
    buttonEvent(incident, index, row) {
      this[incident](index, row);
    },
    pageSizeChange(val) {
      this.filter.per_page = val;
    },
    pageCurrentChange(val) {
      this.filter.page = val;
    },
    /***列表的回调事件 结束**/

    //查看用户信息
    viewData(index, row) {
      this.random = getYearMonDateInt();
      this.dialogViewVisible = true;
      this.currentId = row.id;
    },
     //新增用户信息
    onAdd(){
      this.random = getYearMonDateInt();
      this.dialogAddVisible = true;
    },
    //编辑用户信息
    EditData(index,row){
      this.random = getYearMonDateInt();
      this.dialogEditVisible = true;
      this.currentId = row.id;
    },
    //自定义回调事件
    SuccessReturn(data){
      //操作类型
        switch(data.type){
          case "add":
            this.dialogAddVisible=false;
            break;
          case "edit":
            this.dialogEditVisible=false;
            break;
          case "view":
            this.dialogViewVisible=false;
            break;
          default:
            //下标方式进行关闭
            this[data.type]=false;
            break;
        }
        //是否查询
        if(data.isSelect){
           this.onSubmit();
        }
    },
  }
};
