export const mixinEditMixed={
    data(){
        return{
            //表单
            formInline: {}
        }
    },
    methods:{
     //取消
      EditCancle() {
        //回调事件
        this.$emit("SuccessReturn", { type: "edit", isSelect: false });
      },
      //默认
      EditClear() {
        this.InitFormInLine();
      }
    }
}