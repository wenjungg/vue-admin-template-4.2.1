import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/UseInfo/DoLogin',
    method: 'post',
    data
  })
}

export function logout() {
  return request({
    url: '/UseInfo/OutLogin',
    method: 'post'
  })
}


/**获取用户详情 **/
export function getInfo(token) {
  return request({
    url: '/UseInfo/GetUserInfo',
    method: 'get',
    params: { token }
  })
}

/**
 * 获取用户列表
 * @param {*} value 
 */
export function getUserInfoList(value){
  return request({
    url: '/UseInfo/GetUserInfoList',
    method: 'get',
    params: value
  })
}

/**
 * 添加用户接口
 * @param {*} data 
 */
export function AddEditUserInfo(data){
  return request({
    url: '/UseInfo/AddEditUserInfo',
    method: 'post',
    data
  })
}

export function GetUserInfoSingle(data){
  return request({
    url: '/UseInfo/GetUserInfoSingle',
    method: 'get',
    params: {Id:data} 
  })
}


export function UpdateUserPassword(data){
  return request({
    url: '/UseInfo/ResetPassWord',
    method: 'post',
     data
  })
}



