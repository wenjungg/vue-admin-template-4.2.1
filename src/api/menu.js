
import request from '@/utils/request'

//当前用户自己菜单权限
export function GetMenuList() {
  return request({
    url: '/Menu/GetCurrentMenuList',
    method: 'get'
  })
}
//菜单列表
export function GetMenuInfoList(data){
  return request({
    url: '/Menu/GetMenuInfoList',
    method: 'get',
    params:data
  })
}

//菜单添加编辑
export function AddEditMenuInfo(data){
  return request({
    url: '/Menu/AddEditMenuInfo',
    method: 'post',
    data
  })
}
//菜单详情
export function GetMenuInfo(data){
  return request({
    url: '/Menu/GetMenuInfo',
    method: 'get',
    params:data
  })
}



