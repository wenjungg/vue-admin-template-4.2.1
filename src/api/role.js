

import request from '@/utils/request'
//获取列表
export function GetRoleInfoList(params) {
  return request({
    url: '/Role/GetRoleInfoList',
    method: 'get',
    params
  })
}
//添加，或者编辑
export function AddEditRoleInfo(data){
    return request({
        url: '/Role/AddEditRoleInfo',
        method: 'post',
        data
    })
}
//获取单条信息 id
export function GetRoleInfo(data){
    return request({
        url: '/Role/GetRoleInfo',
        method: 'get',
        params:data
    }) 
}
