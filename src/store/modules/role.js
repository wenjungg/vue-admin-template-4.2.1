import {GetRoleInfoList,AddEditRoleInfo,GetRoleInfo} from '@/api/role';

const state = {
   
  }
  
const mutations = {
   
}
  
const actions = {
     // GetRoleInfoList
     GetRoleInfoList({ commit, state },data) {
        return new Promise((resolve, reject) => {
            GetRoleInfoList(data).then(response => {
            const { data } = response
            resolve(data)
          }).catch(error => {
            reject(error)
          })
        })
      },
    // AddEditRoleInfo
     AddEditRoleInfo({ commit, state },data) {
        return new Promise((resolve, reject) => {
            AddEditRoleInfo(data).then(response => {
            const { data } = response
            resolve(data)
          }).catch(error => {
            reject(error)
          })
        })
    },
    //获取单条信息
    GetRoleInfo({commit,state},data){
        return new Promise((resolve,reject)=>{
            GetRoleInfo(data).then(response => {
                const { data } = response
                resolve(data)
              }).catch(error => {
                reject(error)
              })
        })
    }
}

  
export default {
    namespaced: true,
    state,
    mutations,
    actions
}