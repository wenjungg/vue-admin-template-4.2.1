import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true,
  },
  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true,
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: 'Dashboard', icon: 'dashboard',permission:'public'}
    }]
  },

  /*****************************************系统设置  start *******************************/
  {
    path: '/base',
    component: Layout,
    redirect: '/base/user',
    name: 'sys_setting',
    meta: {
      title: 'setting',
      icon: 'nested',
      permission:'sys_setting'
    },
    children: [
      {
        path: 'user',
        component: () => import('@/views/base/user/index'), // Parent router-view
        name: 'user',
        meta: { title: 'user',icon: 'tree',permission:'sys_set_user' },
      },
      {
        path: 'role',
        component: () => import('@/views/base/role/index'), // Parent router-view
        name: 'role',
        meta: { title: 'role',icon: 'link',permission:'sys_set_role' },
      },
      {
        path: 'menu',
        component: () => import('@/views/base/menu/index'), // Parent router-view
        name: 'menu',
        meta: { title: 'menu',icon: 'example',permission:'sys_set_menu' },
      }
    ]
  },
  /*****************************************系统设置  end *******************************/



  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
   mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router issues

  //刷新也面
  //location.reload();
}

export const asyncRouterMap = [
    //  {
    //     path: '/user',
    //     component: Layout,
    //     redirect: '/user/index',
    //     children: [{
    //         path: 'index',
    //         component: () =>
    //             import('@/views/base/user/index'),
    //         name: 'user-list',
    //         meta: {
    //             title: 'userList',
    //             icon: 'icon',
    //             noCache: true,
    //         }
    //     }]
    // }
];

export default router
